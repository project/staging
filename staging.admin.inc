<?php
/*
 * @file
 * The admin control functions of staging modules.
 * 
 */


function staging_admin($form_state) {
  
  $form['tracing'] = array(
    '#type' => 'checkbox',
    '#title' => t('Switch on tracing queries.'),
    '#default_value' => variable_get('dev_query', 0),
  );
  
  $form['production_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Production site settings'),
    '#collapsible' => FALSE,
  );
  
  $form['production_fieldset']['production_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Production domain'),
    '#description' => t('Example, www.example.com'),
    '#default_value' => variable_get('production_domain', 'localhost'),
  );
  
  $form['production_fieldset']['production_database'] = array(
    '#type' => 'textfield',
    '#title' => t('Production database name'),
    '#description' => t('The name of the database.'),
    '#default_value' => variable_get('production_database', ''),
  );
  
  $form['production_fieldset']['production_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Production domain database username'),
    '#description' => t('The username that used to login the database.'),
    '#default_value' => variable_get('production_username', ''),
  );
  
  $form['production_fieldset']['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Production domain database password'),
    '#description' => t('The password that used to login the database. <strong>This password will not be saved for security reasons.</strong>'),
  );
  
  $form['production_fieldset']['production_maintenance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Switch production site to maintaince mode before publish.'),
    '#default_value' => variable_get('production_maintance', 1),
    '#description' => t('Switch on "site maintance" on production site before publish the queries stored. It will be switched back after complete.'),
  );
  
  $form['advanced'] = array(
    '#type' => 'fieldset',
    '#title' => 'Advanced options',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['advanced']['production_clear_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Clear production site\'s cache after publish'),
    '#default_value' => variable_get('production_clear_cache', 1),
  );
  
  
  $form['advanced']['production_exclude'] = array(
    '#type' => 'textfield',
    '#title' => 'Ignore functions\' from storing',
    '#description' => t('Separate functions by space. Example, enter cache_set to ignore cache queries move to production site'),
    '#default_value' => variable_get('production_exclude', ''),
  );
  
  $form['advanced']['production_default_exclude'] = array(
    '#type' => 'textfield',
    '#title' => 'System\'s exclude functions',
    '#description' => t('Execlude list from module.'),
    '#default_value' => 'cache_clear_all cache_set sess_write staging_admin_submit dblog_watchdog block_admin_display_form_submit',
    '#disabled' => TRUE,
    '#size' => 100,
  );
  
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Restore to default'),
  );
  
  return system_settings_form($form);
}

function staging_admin_validate( $form, &$form_state ) {
  
  global $db_type;
  
  //dprint_r($form_state);
  
  if ($form_state['values']['op'] == t('Save settings') ) {
  
    // Decode url-encoded information in the db connection string.
    $url['user'] = urldecode($form_state['values']['production_username']);
    $url['pass'] = isset($form_state['values']['password']) ? urldecode($form_state['values']['password']) : '';
    $url['host'] = urldecode($form_state['values']['production_domain']);
    $url['path'] = urldecode($form_state['values']['production_database']);
  
    // Allow for non-standard MySQL port.
    if (isset($form_state['values']['port'])) {
      $url['host'] = $url['host'] .':'. $form_state['values']['port'];
    }
  
    // Test connecting to the database.
    $function = $db_type .'_connect';
    $error_function = $db_type .'_error';
    $connection = @$function($url['host'], $url['user'], $url['pass'], $url['path']);
    if (!$connection) {
      form_set_error('', t('Failed to connect to your database server. Reports the following message: %error.<ul><li>Are you sure you have the correct username and password?</li><li>Are you sure that you have typed the correct database hostname?</li><li>Are you sure that the database server is running?</li></ul>', array('%error' => '')));
    }
  }  
}

function staging_admin_submit( $form, &$form_state ) {
  
  global $active_db, $db_url, $db_type, $conf;
  
  $prefix = 'production_';
  
  if ($form_state['values']['op'] == t('Save settings')) {
    
    if ( variable_get('dev_query', 0) ) {
      if ( !$form_state['values']['tracing'] ) {
        watchdog('staging', 'Staging tracing stopped.');
      }
    }
    else {
      if ( $form_state['values']['tracing'] ) {
        watchdog('staging', 'Staging tracing started.');
      }
    }
    
    //ignore this form submit from tracing first, then store the variable_set queries, enable the tracing again(if needed.)
    $conf['dev_query'] = 0;
    variable_set($prefix .'domain', $form_state['values']['production_domain']);
    variable_set($prefix .'database', $form_state['values']['production_database']);
    variable_set($prefix .'username', $form_state['values']['production_username']);
    variable_set($prefix .'maintance', $form_state['values']['production_maintance']);
    variable_set($prefix .'exclude', $form_state['values']['production_exclude']);
    variable_set($prefix .'clear_cache', $form_state['values']['production_clear_cache']);
    variable_set('dev_query', $form_state['values']['tracing']);
    drupal_set_message(t('Production site settings saved.'));
    
  }else if( $form_state['values']['op'] == t('Restore to default') ) {
    $conf['dev_query'] = 0;
    variable_del($prefix .'database');
    variable_del($prefix .'username');
    variable_del($prefix .'maintance');
    variable_del($prefix .'exclude');
    variable_del($prefix .'clear_cache');
    variable_del('dev_query');
  }
}

function staging_admin_publish( $form_state ) {
  
  $form['production_fieldset']['password'] = array(
    '#type' => 'textfield',
    '#title' => t('Production domain database password'),
    '#description' => t('The password that used to login the database. <strong>This password will not be saved for security reasons.</strong>'),
  );
  
  $form['queries_fieldset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Queries stored'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  
  $form['queries_fieldset']['queries'] = array(
    '#type' => 'markup',
    //'#value' => drupal_get_form('staging_query_table'),
    '#value' => staging_query_table(),
  );
  
  $form['publish'] = array(
    '#type' => 'submit',
    '#value' => t('Publish queries to production'),
  );
  
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete stored queries'),
  );
  
  return $form;
}

function staging_admin_publish_validate( $form, &$form_state ) {
  
  global $db_type;
      
  if ($form_state['values']['op'] == t('Publish queries to production') ) {
  
    $prefix = 'production_';
    
    // Decode url-encoded information in the db connection string.
    $url['user'] = variable_get($prefix .'username', '');
    $url['pass'] = isset($form_state['values']['password']) ? urldecode($form_state['values']['password']) : '';
    $url['host'] = variable_get($prefix .'domain', '');
    $url['path'] = variable_get($prefix .'database', '');
  
    // Allow for non-standard MySQL port.
    if (isset($form_state['values']['port'])) {
      $url['host'] = $url['host'] .':'. $form_state['values']['port'];
    }
  
    // Test connecting to the database.
    $function = $db_type .'_connect';
    $error_function = $db_type .'_error';
    $connection = @$function($url['host'], $url['user'], $url['pass'], $url['path']);
    if (!$connection) {
      form_set_error('', t('Failed to connect to your database server. Reports the following message: %error.<ul><li>Are you sure you have the correct username and password?</li><li>Are you sure that you have typed the correct database hostname?</li><li>Are you sure that the database server is running?</li></ul>', array('%error' => '')));
    }
  }  
}

function staging_admin_publish_submit( $form, &$form_state ) {
  
  global $active_db, $db_url, $db_type, $conf;
  
  $prefix = 'production_';
  
  $username = variable_get($prefix .'username', '');
  $domain = variable_get($prefix .'domain', '');
  $database = variable_get($prefix .'database', '');
  $maintenance = variable_get($prefix .'maintenance', '');
  $clear_cache = variable_get($prefix .'clear_cache', '');
  
  if ($form_state['values']['op'] == t('Delete stored queries')) {
    db_query('DELETE FROM {staging_queries}');
    drupal_set_message(t('Queries deleted.'));
  }
  elseif ($form_state['values']['op'] == t('Publish queries to production')) {
    //found the queries to be pushed
    $results = db_query("SELECT * FROM {staging_queries}" );
    while ( $array = db_fetch_array($results) ) {
      $queries[] = $array;
    }
    
    //init the production database
    $org_url = $db_url;
    $password = isset($form_state['values']['password']) ? urldecode($form_state['values']['password']) : '';
    $db_url = array(
      'default' => $org_url,
      'production' => $db_type ."://". $username . $password ."@". $domain ."/". $database, 
    );
    db_set_active('production');
    
    //push queries.
    //offline mode?
    if ( $maintenance ) {
      $serial = serialize($maintenance);
      db_query("UPDATE {variable} SET value = '$serial' WHERE name = 'site_offline';");
    }
    
    $success = TRUE;
    
    foreach ( $queries as $query ) {
      $temp = db_query(str_replace("&#039;", "'", $query['query']));
      if ( !$temp ){
        $error[] = preg_replace(array("/'.*'/s", "/\d.*\.\d.*/", "/\d.*/"), array("S", "F", "D"), $query['query']);
      }
      $success = $success && $temp;
    }
    if ($success) {
      watchdog('staging', t("@number queries incoming from stage server.", array('@number' => count($queries)) ) );
    }
    else {
      watchdog('error', t('There are errors when importing queries from stage. Error: %error', array('%error' => implode(' ',$error))));
    }
    
    if ( $maintenance ) {
      $serial = serialize(0);
      db_query("UPDATE {variable} SET value = '$serial' WHERE name = 'site_offline';");
    }
    if ( $clear_cache ) {
      drupal_flush_all_caches();
    }
    
    //restore the stage database
    db_set_active();
    if ($success) {
      db_query('DELETE FROM {staging_queries}');
      drupal_set_message( t("@number queries pushed to production server: !production", array( '@number' => count($queries), '!production' => $db_url['production']) ) );
      watchdog('staging', t("@number queries incoming from stage server.", array('@number' => count($queries)) ) );
    }
    else {
      $error = implode(' ',$error);
      drupal_set_message( t('There are errors when importing queries from stage. Error: %error', array('%error' => $error)), 'error' );
      watchdog('error', t('There are errors when importing queries from stage. Error: %error', array('%error' => $error)));
    }
    
    //clean up
    $db_url = $org_url;
  }
  
}