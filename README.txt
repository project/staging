﻿LICENSE:
GPL license

MODULE NAME:
staging

ABOUT:
Staging module provides methods to synchronize two server databases.


/Usage/
Start by activating "Switch on tracing" this makes the module trace
every query performed behind the scenes.
The queries are stored and pushed to the second server upon request.


This helps the process of migrating the data between stage and
production servers.

CONTACT:
author: joetsuihk@gmail.com
website: http://www.joetsuihk.com
